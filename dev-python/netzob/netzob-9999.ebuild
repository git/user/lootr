# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python{2_6,2_7} )

inherit eutils distutils-r1 git-2 python-r1

MY_P="Netzob-${PV}"

DESCRIPTION="Network protocol modelization by reverse engineering"
HOMEPAGE="http://www.netzob.org/"
EGIT_REPO_URI="https://dev.netzob.org/git/netzob.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="doc debug"

RDEPEND="
	x11-libs/gtk+:3
	dev-python/impacket
	dev-util/strace
	sys-process/lsof
	>=dev-python/lxml-2.3
	dev-python/bitarray
	dev-python/Babel
	dev-python/pygobject:3
	dev-python/pcapy
	dev-python/python-ptrace
	>=dev-python/httplib2-0.7.0
	media-gfx/graphviz"
DEPEND="${RDEPEND}
	>=dev-python/setuptools-0.6.30-r1
	doc? ( >=dev-python/sphinx-1.1.3 )"

EGIT_BRANCH="next"

S="${WORKDIR}/${MY_P}"
DOCS=( README.rst AUTHORS.txt NEWS.rst COPYING.txt )

src_compile() {
	use debug && export NETZOB_COMPILE_PROFILE="debug"
	distutils-r1_src_compile

	if use doc; then
		einfo "Generation of documentation"
		pushd doc/documentation >/dev/null
		sphinx-build -b html source html || die "Generation of documentation failed"
		popd >/dev/null
	fi
}

src_install() {
	distutils-r1_src_install
	use doc && HTML_DOCS=( doc/documentation/html/ )
	doman doc/netzob.1
}
